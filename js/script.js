'use strict'
// Fonction utilitaire

function arrondiradeuxchiffre (nombre) {
  return Math.floor(nombre * 100) / 100
}

// Profil utilisateur

const profil = {
  nom: 'Gagnon',
  prenom: 'Jeff',
  dateNaissance: new Date(1985, 5, 12),
  sexe: 'H',
  situationParticuliere: false
}

// Nombre de jours a analyser

let nbJours = prompt('Combien de jours voulez-vous analyser?')
nbJours = parseInt(nbJours)

// Nombre de consommation par jour

const journeesAlcool = []

for (let jour = 1; jour <= nbJours; jour++) {
  let nbConsommationAlcool = prompt('Entrez le nombre de consommation d\'alcool pour jour ' + jour)
  nbConsommationAlcool = parseInt(nbConsommationAlcool)
  journeesAlcool.push(nbConsommationAlcool)
}

// Recommandation par jour/semaine selon sexe && situation

const recommandation = {
  maxAlcoolJour: 2,
  maxAlcoolSemaine: 10
}

if (profil.situationParticuliere === true) {
  recommandation.maxAlcoolSemaine = 0
  recommandation.maxAlcoolJour = 0
} else if (profil.sexe === 'H') {
  recommandation.maxAlcoolSemaine = 15
  recommandation.maxAlcoolJour = 3
}

// Calcul de l'age de l'utilisateur

const DATE = new Date()
const age = Math.floor((DATE - profil.dateNaissance) / (1000 * 60 * 60 * 24 * 365.25))

// Moyenne des consommations

let totalConsommation = 0

for (let i = 0; i < journeesAlcool.length; i++) {
  totalConsommation = totalConsommation + journeesAlcool[i]
}

let moyenne = totalConsommation / nbJours
moyenne = arrondiradeuxchiffre(moyenne)

// Somme de consommation hebdomadaire

const sommeHebdo = Math.round(moyenne * 7)

// Consommation la plus grande en une journée

const max = Math.max(...journeesAlcool)

// Recommandation sont oui ou non respectee

let recommandationAlcoolRespectee = true

if (sommeHebdo > recommandation.maxAlcoolSemaine) {
  recommandationAlcoolRespectee = false
} else if (max > recommandation.maxAlcoolJour) {
  recommandationAlcoolRespectee = false
}

// Ratio des journées avec consommations excédantes et des journées à zéro consommation

let nbJoursExcedant = 0
let nbJoursZero = 0

for (let i = 0; i < journeesAlcool.length; i++) {
  if (journeesAlcool[i] > recommandation.maxAlcoolJour) {
    nbJoursExcedant++
  } if (journeesAlcool[i] === 0) {
    nbJoursZero++
  }
}

nbJoursExcedant = arrondiradeuxchiffre(nbJoursExcedant / nbJours * 100)
nbJoursZero = arrondiradeuxchiffre(nbJoursZero / nbJours * 100)

// Rapport de l'analyse

console.log('Rapport de l\'analyse')
console.log(DATE.toLocaleString())
console.log(`${profil.nom}, ${profil.prenom}`)
console.log(`Âge : ${age} ans`)
console.log('Alcool :')
console.log(`Moyenne par jour : ${moyenne}`)
console.log(`Consommation sur une semaine : ${sommeHebdo}  Maximum recommandé : ${recommandation.maxAlcoolSemaine}`)
console.log(`Consommation la plus haute en une journée : ${max}  Maximum recommandé : ${recommandation.maxAlcoolJour}`)
console.log('Ratio des journées avec consommations excédantes : ' + nbJoursExcedant + '%')
console.log('Ratio des journées avec zéro consommation : ' + nbJoursZero + '%')
if (recommandationAlcoolRespectee) {
  console.log('Vous respectez les recommandations')
} else {
  console.log('Vous ne respectez pas les recommandations')
}